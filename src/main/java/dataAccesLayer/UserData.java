package dataAccesLayer;

import dataAccesLayer.DatabaseConnect;
import model.Client;
import model.User;

import java.sql.*;

public class UserData {


    public void addUser(Client client, User user) {
        System.out.println(user.getUsername() + " " + user.getUsername() + " " + client.getID());
        Connection connection = DatabaseConnect.getConnection();
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement("insert into _user (clientID, username, passwd) values (?, ?, ?)");
            statement.setInt(1, client.getID());
            statement.setString(2, user.getUsername());
            statement.setString(3, user.getPassword());
            statement.execute();

            statement.execute("select count(*) from _user");
            ResultSet resultSet = statement.getResultSet();
            while (resultSet.next()) {
                user.setID(resultSet.getInt(1));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DatabaseConnect.close(connection);
            DatabaseConnect.close(statement);
        }
    }

    public int loginTry(String username, String passwd) {
        int valid = 0;
        try {
            Connection connection = DatabaseConnect.getConnection();
            CallableStatement statement = connection.prepareCall("CALL login(?, ?, ?);");
            statement.setString(1, username);
            statement.setString(2, passwd);
            statement.setInt(3, valid);

            statement.execute();
            valid = statement.getInt(3);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return valid;
    }
}
