package dataAccesLayer;

import model.OrderDetails;
import model.Product;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.sql.*;
import java.util.Date;
import java.text.SimpleDateFormat;
import java.util.List;


public class OrderData {

    private void createBill(int clientID, List<OrderDetails> orderDetails) {
        try {
//            PrintWriter writer = new PrintWriter("bills.txt", "UTF-8");
            Files.write(Paths.get("bills.txt"), ( "Order ID: " + orderDetails.get(0).getOrderID() + "\n").getBytes() ,StandardOpenOption.APPEND);
            Files.write(Paths.get("bills.txt"), ("Client ID: " + clientID + "\n").getBytes() ,StandardOpenOption.APPEND);
            float total = 0.0f;
            for(OrderDetails orderDetails1: orderDetails) {
                Product product = new AbstractAccess<>(Product.class).getColumnByID(orderDetails1.getProductID());
                Files.write(Paths.get("bills.txt"), ("\t" + "Product: " + product.getProductName() + "\t" + product.getProductPrice() + "\t" + "x" + orderDetails1.getAmount() + "\n").getBytes() ,StandardOpenOption.APPEND);
                total += product.getProductPrice();
            }

            Files.write(Paths.get("bills.txt"), ("Total price: " + total + "\n").getBytes() ,StandardOpenOption.APPEND);
            Files.write(Paths.get("bills.txt"), ("---------------------------------------------------------------" + "\n").getBytes() ,StandardOpenOption.APPEND);


        } catch (FileNotFoundException | UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void addOrder(int clientID, List<OrderDetails> orderDetails) {
        Connection connection = DatabaseConnect.getConnection();
        int orderID = 0;
        float orderCost = 0.0f;
        try {
            /*
            adaugam comanda in baza de date
            initial setam costul total la 0
             */
            PreparedStatement statement = connection.prepareStatement("insert into _order (clientID, orderCost, orderDate) values (?, ?, ?)");
            statement.setInt(1, clientID);
            statement.setFloat(2, 0.0f);
            statement.setString(3, (new SimpleDateFormat("yyyy-MM-dd")).format(new Date()));
            statement.execute();

            /*
            luam ID-ul ultimei comenzi adaugate (comanda curenta)
             */
            PreparedStatement statement1 = connection.prepareStatement("select count(*) from _order");
            ResultSet resultSet = statement1.executeQuery();
            while (resultSet.next()) {
                orderID = resultSet.getInt(1);
            }

            /*
            adaugam in detaliile comenzii toate produsele comandate
             */
            statement1 = connection.prepareStatement("insert into orderDetails (orderID, productID, amount) values (?, ?, ?)");
            for (OrderDetails od : orderDetails) {
                od.setOrderID(orderID);
                statement1.setInt(1, orderID);
                statement1.setInt(2, od.getProductID());
                statement1.setInt(3, od.getAmount());
                statement1.execute();
                PreparedStatement statement2 = connection.prepareStatement("update Product set productQuantity = productQuantity - ? where ID = ?;");
                statement2.setInt(1, od.getAmount());
                statement2.setInt(2, od.getProductID());
                statement2.execute();
                orderCost += od.getAmount() * (new ProductData().getProduct(od.getProductID()).getProductPrice());
            }

            // in cele din urma, setam pretul total al comenzii
            statement = connection.prepareStatement("update _order set orderCost = ? where ID = ?");
            statement.setFloat(1, orderCost);
            statement.setInt(2, orderID);
            statement.execute();

            DatabaseConnect.close(statement);
            DatabaseConnect.close(statement1);
            DatabaseConnect.close(resultSet);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        DatabaseConnect.close(connection);
        createBill(clientID, orderDetails);
    }
}
