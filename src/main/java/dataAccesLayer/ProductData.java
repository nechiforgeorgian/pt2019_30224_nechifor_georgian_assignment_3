package dataAccesLayer;

import model.Product;

import java.sql.*;

public class ProductData {

    public Product getProduct(int id) {
        Product product = null;
        Connection connection = DatabaseConnect.getConnection();
        try {
            PreparedStatement st = connection.prepareStatement("select * from Product where Product.ID = ?");
            st.setInt(1, id);
            ResultSet resultSet = st.executeQuery();

            while (resultSet.next()) {
                product = new Product(resultSet.getInt(1), resultSet.getString(2), resultSet.getInt(3), resultSet.getFloat(4));
            }
            DatabaseConnect.close(resultSet);
            DatabaseConnect.close(st);
            DatabaseConnect.close(connection);

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return product;
    }

    public void deleteProduct(int id) {
        Connection connection = DatabaseConnect.getConnection();
        try {
            PreparedStatement statement = connection.prepareStatement("update Product set productQuantity = 0 where Product.ID = ?");
            statement.setInt(1, id);
            statement.execute();

            DatabaseConnect.close(statement);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        DatabaseConnect.close(connection);
    }

    public void addStock(int id, int Stock) {
        Connection connection = DatabaseConnect.getConnection();
        try {
            PreparedStatement statement = connection.prepareStatement("update Product set Product.productQuantity = ? where ID = ?");
            statement.setInt(1, Stock);
            statement.setInt(2, id);
            statement.execute();

            DatabaseConnect.close(statement);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        DatabaseConnect.close(connection);
    }

    public void addProduct(Product product) {
        Connection connection = DatabaseConnect.getConnection();
        try {
            PreparedStatement statement = connection.prepareStatement("insert into Product (productName, productQuantity, productPrice) values (?, ?, ?);");
            statement.setString(1, product.getProductName());
            statement.setInt(2, product.getProductQuantity());
            statement.setFloat(3, product.getProductPrice());
            statement.execute();

            ResultSet resultSet = statement.executeQuery("select count(*) from Product");
            while (resultSet.next()) {
                product.setId(resultSet.getInt(1));
            }

            DatabaseConnect.close(resultSet);
            DatabaseConnect.close(statement);

        } catch (SQLException e) {
            e.printStackTrace();
        }

        DatabaseConnect.close(connection);
    }

    public int getNumber() {
        int number = 0;
        Connection connection = DatabaseConnect.getConnection();
        try {
            PreparedStatement statement = connection.prepareStatement("select count(*) from product;");
            ResultSet resultSet = statement.executeQuery();
            while(resultSet.next())
                number =  resultSet.getInt(1);
        }catch (SQLException e) {
            e.printStackTrace();
        }
        finally {
            return number;
        }
    }

}
