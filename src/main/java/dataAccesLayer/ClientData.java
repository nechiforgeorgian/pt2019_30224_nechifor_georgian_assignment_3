package dataAccesLayer;

import dataAccesLayer.DatabaseConnect;
import model.Client;

import java.sql.*;

public class ClientData {

    public void addClient(Client c) {
        Connection connection = DatabaseConnect.getConnection();
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            statement = connection.prepareStatement("insert into client (clientName, clientEmail, clientPhoneNumber, clientRegistrationDate) values (?, ?, ?, ?);");
            statement.setString(1, c.getClientName());
            statement.setString(2, c.getClientEmail());
            statement.setString(3, c.getClientPhoneNumber());
            statement.setDate(4, (Date) c.getClientRegistrationDate());
            statement.execute();

            statement.execute("select count(*) from client");
            resultSet = statement.getResultSet();
            while (resultSet.next()) {
                c.setID(resultSet.getInt(1));
            }


        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DatabaseConnect.close(connection);
            DatabaseConnect.close(statement);
            DatabaseConnect.close(resultSet);
        }
    }

    public void deleteClient(int id) {
        Connection connection = DatabaseConnect.getConnection();
        try {
            PreparedStatement statement = connection.prepareStatement("update client set clientName = 'null' where ID = ?");
            statement.setInt(1, id);
            statement.execute();

            statement = connection.prepareStatement("delete from _user where clientID = ?");
            statement.setInt(1, id);
            statement.execute();

            DatabaseConnect.close(connection);
            DatabaseConnect.close(statement);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public Client editClient(Client c) {
        Client client = new Client();
        Connection connection = DatabaseConnect.getConnection();
        try {
            PreparedStatement statement = connection.prepareStatement("update client set clientName = ?, clientEmail = ?, clientPhoneNumber = ? where ID = ?;");
            statement.setString(1, c.getClientName());
            statement.setString(2, c.getClientEmail());
            statement.setString(3, c.getClientPhoneNumber());
            statement.setInt(4, c.getID());
            statement.execute();
            statement.close();

            statement = connection.prepareStatement("select * from client where ID = ?");
            statement.setInt(1, c.getID());
            statement.execute();
            ResultSet resultSet = statement.getResultSet();

            while(resultSet.next()) {
                client.setID(resultSet.getInt(1));
                client.setClientName(resultSet.getString(2));
                client.setClientEmail(resultSet.getString(3));
                client.setClientPhoneNumber(resultSet.getString(4));
                client.setClientRegistrationDate(resultSet.getDate(5));
                        //= new Client(resultSet.getInt(1), resultSet.getString(2), resultSet.getDate(5),
                          //  resultSet.getString(3), resultSet.getString(4));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return client;
    }

    public int getNumber() {
        int number = 0;
        Connection connection = DatabaseConnect.getConnection();
        try {
            PreparedStatement statement = connection.prepareStatement("select count(*) from client;");
            ResultSet resultSet = statement.executeQuery();
            while(resultSet.next())
                number =  resultSet.getInt(1);
        }catch (SQLException e) {
            e.printStackTrace();
        }
        finally {
            return number;
        }
    }
}
