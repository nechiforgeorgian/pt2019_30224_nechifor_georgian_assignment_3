package businessLogicLayer;

import dataAccesLayer.ProductData;
import model.Product;

public class ProductBusiness {

    public Product getProduct(Product product) {
        if (product != null) {
            if (product.getId() > 0) {
                return new ProductData().getProduct(product.getId());
            }
        }
        return null;
    }

    public boolean deleteProduct(Product product) {
        if (product != null) {
            new ProductData().deleteProduct(product.getId());
            return true;
        }
        return false;
    }

    public boolean addStock(Product product, int newStock) {
        if (product != null && newStock > 0) {
            new ProductData().addStock(product.getId(), newStock);
            return true;
        } else
            return false;
    }

    public boolean addProduct(Product product) {
        if (product != null) {
            if (!product.getProductName().isEmpty() && product.getProductPrice() > 0 && product.getProductQuantity() > 0) {
                new ProductData().addProduct(product);
                return true;
            } else
                return false;
        } else
            return false;
    }

    public int getNumber() {
        return new ProductData().getNumber();
    }
}
