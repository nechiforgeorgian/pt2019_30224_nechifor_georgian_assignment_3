package businessLogicLayer;

import dataAccesLayer.UserData;
import model.Client;
import model.User;

public class UserBusiness {

    public boolean addUser(Client client, User user) {
        if(user != null && client != null) {
            if(client.getID() > 0 && !client.getClientName().isEmpty() && client.getClientPhoneNumber().matches("\\d+") && client.getClientEmail().contains("@gmail.com")) {
                try {
                    new UserData().addUser(client, user);
                }catch (Exception ex) {
                    System.out.println("User not added");
                }
                return true;
            }
        }
        return false;
    }

    public int loginTry(String username, String password) {
        if(!username.isEmpty() && !password.isEmpty()) {
            return new UserData().loginTry(username, password);
        }
        return -1;
    }
}
