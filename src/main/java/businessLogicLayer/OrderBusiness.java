package businessLogicLayer;

import dataAccesLayer.OrderData;
import model.Client;
import model.OrderDetails;

import java.util.List;

public class OrderBusiness {

    public void addOrder(Client client, List<OrderDetails> details) {
        if(client != null && !details.isEmpty()) {
            new OrderData().addOrder(client.getID(), details);
        }
    }
}
