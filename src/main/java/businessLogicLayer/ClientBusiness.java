package businessLogicLayer;

import dataAccesLayer.ClientData;
import model.Client;

import java.util.Date;

public class ClientBusiness {

    public boolean addClient(Client c) {
        if (c != null) {
            if (c.getClientName().isEmpty() || !c.getClientEmail().contains("@gmail.com")) {
                return false;
            } else
                new ClientData().addClient(c);
        }
        return true;
    }

    public void deleteClient(Client c) {
        if (c != null) {
            new ClientData().deleteClient(c.getID());
        }
    }

    public boolean editClient(Client c) {
        String emailRegex = "^[a-zA-Z0-9_+&*-]+(?:\\." + "[a-zA-Z0-9_+&*-]+)*@" + "(?:[a-zA-Z0-9-]+\\.)+[a-z" + "A-Z]{2,7}$";
        if (c != null) {
            if (c.getClientName().isEmpty() || !c.getClientPhoneNumber().matches("\\d+") || !c.getClientEmail().matches(emailRegex)) {
                return false;
            }
        } else
            new ClientData().editClient(c);
        return true;
    }

    public int getNumberOfClients() {
        return new ClientData().getNumber();
    }
}
