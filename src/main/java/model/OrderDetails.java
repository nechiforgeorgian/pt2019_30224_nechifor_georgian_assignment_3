package model;

public class OrderDetails {
    private int ID;
    private int orderID;
    private int productID;
    private int amount;

    public OrderDetails(int id, int orderID, int productID, int amount) {
        this.ID = id;
        this.orderID = orderID;
        this.productID = productID;
        this.amount = amount;
    }

    public int getId() {
        return ID;
    }
    public void setId(int id) {
        this.ID = id;
    }

    public int getOrderID() {
        return orderID;
    }

    public void setOrderID(int orderID) {
        this.orderID = orderID;
    }

    public int getProductID() {
        return productID;
    }

    public int getAmount() {
        return amount;
    }

}
