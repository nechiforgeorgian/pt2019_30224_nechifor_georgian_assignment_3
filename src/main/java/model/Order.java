package model;
public class Order {
    private int ID;
    private int clientID;
    private float orderCost;
    private String orderDate;

    public Order(int orderID, int clientID, float orderCost, String orderDate) {
        this.ID = orderID;
        this.clientID = clientID;
        this.orderCost = orderCost;
        this.orderDate = orderDate;
    }

    public int getID() {
        return ID;
    }

    public void setID(int orderID) {
        this.ID = orderID;
    }

    public int getClientID() {
        return clientID;
    }

    public void setClientID(int clientID) {
        this.clientID = clientID;
    }

    public float getOrderCost() {
        return orderCost;
    }

    public void setOrderCost(float orderCost) {
        this.orderCost = orderCost;
    }

    public String getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(String orderDate) {
        this.orderDate = orderDate;
    }
}
