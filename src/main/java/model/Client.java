package model;

import java.util.Date;

public class Client {
    private int ID;
    private String clientName;
    private String clientEmail;
    private String clientPhoneNumber;
    private Date clientRegistrationDate; //data la care s-a inscris clientul FORMAT: YYYY-MM-DD

    public Client() {
        super();
    }

    public Client(int ID, String clientName, Date clientRegistrationDate, String clientEmail, String clientPhoneNumber) {
        this.ID = ID;
        this.clientName = clientName;
        this.clientRegistrationDate = clientRegistrationDate;
        this.clientEmail = clientEmail;
        this.clientPhoneNumber = clientPhoneNumber;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public Date getClientRegistrationDate() {
        return clientRegistrationDate;
    }

    public void setClientRegistrationDate(Date clientRegistrationDate) {
        this.clientRegistrationDate = clientRegistrationDate;
    }

    public String getClientEmail() {
        return clientEmail;
    }

    public void setClientEmail(String clientEmail) {
        this.clientEmail = clientEmail;
    }

    public String getClientPhoneNumber() {
        return clientPhoneNumber;
    }

    public void setClientPhoneNumber(String clientPhoneNumber) {
        this.clientPhoneNumber = clientPhoneNumber;
    }

    public String toString() {
        return this.getClientName() + " / " + this.getClientEmail() + " / "
                + this.getClientPhoneNumber() + " / " + this.getClientRegistrationDate();
    }
}
