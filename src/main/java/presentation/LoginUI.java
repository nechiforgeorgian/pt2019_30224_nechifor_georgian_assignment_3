package presentation;

import businessLogicLayer.UserBusiness;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.logging.Level;
import java.util.logging.Logger;

public class LoginUI {
    private static final Logger LOGGER = Logger.getLogger(AdminUI.class.getName());
    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                LoginUI login = new LoginUI();
                login.initialize();
            }
        });
    }

    void initialize() {
        final JFrame frame = new JFrame();
        frame.setTitle("Login");
        frame.setBounds(100, 100, 500, 250);
        frame.setLayout(null);
        frame.setVisible(true);

        JLabel usernameLabel = new JLabel("Username: ");
        usernameLabel.setBounds(80, 30, 100, 30);
        frame.add(usernameLabel);

        final JTextField username = new JTextField();
        username.setBounds(180, 30, 200, 30);
        frame.add(username);

        JLabel passwordLabel = new JLabel("Password: ");
        passwordLabel.setBounds(80, 80, 100, 30);
        frame.add(passwordLabel);

        final JTextField password = new JPasswordField();
        password.setBounds(180, 80, 200, 30);
        frame.add(password);

        final JButton login = new JButton("Login");
        login.setBounds(75, 150, 150, 30);
        frame.add(login);

        JButton register = new JButton("Register");
        register.setBounds(235, 150, 150, 30);
        frame.add(register);

        username.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                password.requestFocusInWindow();
            }
        });

        password.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                login.doClick();
            }
        });

        login.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                int validation = new UserBusiness().loginTry(username.getText(), password.getText());
                if(validation == -1) {
                    JOptionPane.showMessageDialog(frame, "Username or password incorrect", "Login Failed", JOptionPane.ERROR_MESSAGE);
                } else {
                    frame.dispose();
                    if(validation == 1) {
                        new AdminUI();
                    } else {
                        new ProductsUI(validation, 0);
                    }
                }
            }
        });

        register.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                frame.dispose();
                new RegisterUI(0);
            }
        });
    }
}
