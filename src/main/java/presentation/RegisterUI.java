package presentation;

import businessLogicLayer.ClientBusiness;
import businessLogicLayer.UserBusiness;
import model.Client;
import model.User;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Calendar;
import java.util.Date;

@SuppressWarnings("deprecation")
public class RegisterUI {
    private int nextFrame;

    public RegisterUI(int frame) {
        this.nextFrame = frame;
        EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                initialize();
            }
        });
    }

    private void initialize() {
        JFrame frame = new JFrame("Register");
        frame.setBounds(100, 100, 400, 350);
        frame.setVisible(true);
        frame.setLayout(null);

        JLabel usernameLabel = new JLabel("Username: ");
        usernameLabel.setBounds(30, 20, 100, 30);
        frame.add(usernameLabel);
        JTextField username = new JTextField();
        username.setBounds(150, 20, 200, 30);
        frame.add(username);

        JLabel passwordLabel = new JLabel("Password: ");
        passwordLabel.setBounds(30, 60, 100, 30);
        frame.add(passwordLabel);
        JTextField password = new JPasswordField();
        password.setBounds(150, 60, 200, 30);
        frame.add(password);

        JLabel passwordRepeatLabel = new JLabel("Repeat Password: ");
        passwordRepeatLabel.setBounds(10, 100, 120, 30);
        frame.add(passwordRepeatLabel);
        JTextField passwordRepeat = new JPasswordField();
        passwordRepeat.setBounds(150, 100, 200, 30);
        frame.add(passwordRepeat);

        JLabel nameLabel = new JLabel("Name: ");
        nameLabel.setBounds(30, 140, 120, 30);
        frame.add(nameLabel);
        JTextField name = new JTextField();
        name.setBounds(150, 140, 200, 30);
        frame.add(name);

        JLabel mailLabel = new JLabel("Email: ");
        mailLabel.setBounds(30, 180, 120, 30);
        frame.add(mailLabel);
        JTextField email = new JTextField();
        email.setBounds(150, 180, 200, 30);
        frame.add(email);

        JLabel phoneLabel = new JLabel("Phone Number: ");
        phoneLabel.setBounds(30, 220, 120, 30);
        frame.add(phoneLabel);
        JTextField phoneNumber = new JTextField();
        phoneNumber.setBounds(150, 220, 200, 30);
        frame.add(phoneNumber);

        JButton back = new JButton("Back");
        back.setBounds(30, 270, 120, 30);
        frame.add(back);
        JButton finish = new JButton("Register");
        finish.setBounds(170, 270, 120, 30);
        frame.add(finish);

        back.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                frame.dispose();
                if (nextFrame == 0)
                    new LoginUI().initialize();
            }
        });

        finish.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int year = Calendar.getInstance().get(Calendar.YEAR);
                int month = Calendar.getInstance().get(Calendar.MONTH);
                int day = Calendar.getInstance().get(Calendar.DATE);
                Date date = new java.sql.Date(year - 1900, month, day);

                Client client = new Client(0, name.getText(), date, email.getText(), phoneNumber.getText());
                new ClientBusiness().addClient(client);

                User user = new User(client.getID(), username.getText(), password.getText());
                if(new UserBusiness().addUser(client, user)) {

                    JOptionPane.showMessageDialog(frame, "Account register successfully", "Success", JOptionPane.INFORMATION_MESSAGE);
                    frame.dispose();
                    if (nextFrame == 1)
                        new ClientsUI();
                    else
                        new LoginUI().initialize();
                }
                else
                    JOptionPane.showMessageDialog(frame, "Please fill up all the fields.", "Error", JOptionPane.ERROR_MESSAGE);
            }
        });


    }
}
