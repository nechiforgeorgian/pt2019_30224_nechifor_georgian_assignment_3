package presentation;

import businessLogicLayer.ProductBusiness;
import model.Product;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


public class UpdateProductUI {
    public int closeFrame = 0;
    UpdateProductUI() {
        EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                initialize();
            }
        });
    }

    private void initialize() {
        JFrame frame = new JFrame();
        frame.setLayout(null);
        frame.setBounds(100, 100, 400, 230);
        frame.setTitle("Product List");
        frame.setVisible(true);

        JLabel productIDLabel = new JLabel("Product ID: ");
        productIDLabel.setBounds(30, 30, 150, 30);
        frame.add(productIDLabel);

        JLabel productStockLabel = new JLabel("Product new stock: ");
        productStockLabel.setBounds(30, 90, 150, 30);
        frame.add(productStockLabel);

        JTextField productID = new JTextField();
        productID.setBounds(180, 30, 120, 30);
        frame.add(productID);

        JTextField productStock = new JTextField();
        productStock.setBounds(180, 90, 120, 30);
        frame.add(productStock);

        JButton finish = new JButton("Update Stock");
        finish.setBounds(120, 140, 150, 30);
        frame.add(finish);

        finish.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Product product = new Product();
                try {
                    product.setId(Integer.parseInt(productID.getText()));
                    if (new ProductBusiness().addStock(product, Integer.parseInt(productStock.getText()))) {
                        JOptionPane.showMessageDialog(frame, "Stock updated successfully!", "Success", JOptionPane.INFORMATION_MESSAGE);
                        frame.dispose();
                        closeFrame = 1;
                        new ProductsUI(1, 1);
                    } else {
                        JOptionPane.showMessageDialog(frame, "Update stock failed!", "Error", JOptionPane.ERROR_MESSAGE);
                    }

                } catch (NumberFormatException ex) {
                    JOptionPane.showMessageDialog(frame, "Update stock failed!", "Error", JOptionPane.ERROR_MESSAGE);

                }

            }
        });
    }
}
