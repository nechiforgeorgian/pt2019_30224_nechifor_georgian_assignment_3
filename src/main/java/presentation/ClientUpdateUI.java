package presentation;

import businessLogicLayer.ClientBusiness;
import model.Client;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ClientUpdateUI {
    Client client;

    ClientUpdateUI(Client client) {
        this.client = client;
        EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                initialize();
            }
        });
    }

    private void initialize() {
        JFrame frame = new JFrame("Update Client");
        frame.setLayout(null);
        frame.setVisible(true);
        frame.setBounds(100, 100, 350, 250);
        JTextField name = new JTextField(client.getClientName());
        name.setBounds(50, 10, 250, 30);
        frame.add(name);

        JTextField email = new JTextField(client.getClientEmail());
        email.setBounds(50, 60, 250, 30);
        frame.add(email);

        JTextField phoneNumber = new JTextField(client.getClientPhoneNumber());
        phoneNumber.setBounds(50, 110, 250, 30);
        frame.add(phoneNumber);

        JButton update = new JButton("Update");
        update.setBounds(100, 160, 150, 30);
        frame.add(update);
        update.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                client.setClientName(name.getText());
                client.setClientEmail(email.getText());
                client.setClientPhoneNumber(phoneNumber.getText());
                new ClientBusiness().editClient(client);
                frame.dispose();
                new ClientsUI();
            }
        });
    }
}
