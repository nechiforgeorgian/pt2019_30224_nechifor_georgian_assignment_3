package presentation;

import businessLogicLayer.ProductBusiness;
import model.Product;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class AddProductUI {
    private JFrame frame;
    private int clientID;

    AddProductUI(int clientID) {
        this.clientID = clientID;
        EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                initialize();
            }
        });
    }

    private void initialize() {
        frame = new JFrame();
        frame.setVisible(true);
        frame.setLayout(null);
        frame.setTitle("Add Product");
        frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        frame.setBounds(100, 100, 400, 300);

        JLabel productNameLabel = new JLabel("Product name: ");
        productNameLabel.setBounds(30, 30, 150, 30);
        frame.add(productNameLabel);

        JLabel productQuantityLabel = new JLabel("Product Quantity: ");
        productQuantityLabel.setBounds(30, 90, 150, 30);
        frame.add(productQuantityLabel);

        JLabel productPriceLabel = new JLabel("Product Price: ");
        productPriceLabel.setBounds(30, 150, 150, 30);
        frame.add(productPriceLabel);

        JTextField productName = new JTextField();
        productName.setBounds(180, 30, 150, 30);
        frame.add(productName);

        JTextField productQuantity = new JTextField();
        productQuantity.setBounds(180, 90, 150, 30);
        frame.add(productQuantity);

        JTextField productPrice = new JTextField();
        productPrice.setBounds(180, 150, 150, 30);
        frame.add(productPrice);

        JButton finish = new JButton("ADD PRODUCT");
        finish.setBounds(90, 200, 200, 30);
        frame.add(finish);

        finish.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Product product;
                try {
                    product = new Product(0, productName.getText(), Integer.parseInt(productQuantity.getText()), Float.parseFloat(productPrice.getText()));
                } catch (NumberFormatException ex) {
                    JOptionPane.showMessageDialog(frame, "Please fill all the fields with correct info", "Error", JOptionPane.ERROR_MESSAGE);
                    product = null;
                }
                if (new ProductBusiness().addProduct(product)) {
                    JOptionPane.showMessageDialog(frame, "Product added successfully", "Success", JOptionPane.INFORMATION_MESSAGE);
                    frame.dispose();
                    new ProductsUI(clientID, 1);
                } else
                    JOptionPane.showMessageDialog(frame, "Please fill all the text inputs", "Error", JOptionPane.ERROR_MESSAGE);
            }
        });
    }
}
