package presentation;

import businessLogicLayer.ClientBusiness;
import dataAccesLayer.AbstractAccess;
import model.Client;

import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.Field;

public class ClientsUI {
    private JTable clients;
    private JFrame frame;
    private static int deleted = 0;
    ClientsUI() {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                initialize();
            }
        });
    }

    private void initialize() {
        frame = new JFrame();
        frame.setLayout(null);
        frame.setBounds(100, 100, 700, 400);
        frame.setTitle("Clients List");
        frame.setVisible(true);
        (new Task()).execute();

        JButton addClient = new JButton("Add Client");
        addClient.setBounds(30, 260, 150, 30);
        frame.add(addClient);
        addClient.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                frame.dispose();
                new RegisterUI(1);

            }
        });

        JButton deleteClient = new JButton("Delete Selected Client");
        deleteClient.setBounds(200, 260, 200, 30);
        frame.add(deleteClient);
        deleteClient.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (clients.getSelectedRow() != -1) {
                    deleted++;
                    if((clients.getModel().getValueAt(clients.getSelectedRow(), 1)) != null)
                        try {
                            Client client = new Client();
                            client.setID(Integer.parseInt(clients.getModel().getValueAt(clients.getSelectedRow(), 0).toString()));
                            new ClientBusiness().deleteClient(client);

                        } catch (NullPointerException ex) {
                            System.out.println("Alege un client.");

                        }
                    frame.dispose();
                    new ClientsUI();

                } else {
                    System.out.println("Select a client to delete");
                }
            }
        });

        JButton editClient = new JButton("Edit Client");
        editClient.setBounds(420, 260, 150, 30);
        frame.add(editClient);
        editClient.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (clients.getSelectedRow() > 0 && clients.getSelectedRow() < clients.getRowCount()-deleted) {
                    int id = 0;
                    try {
                         id = (int) clients.getModel().getValueAt(clients.getSelectedRow(), 0);
                    }catch (NumberFormatException ex) {
                        JOptionPane.showConfirmDialog(frame, "Choose a client to delete.", "Error", JOptionPane.ERROR_MESSAGE);
                        return;
                    }

                    Client c = new AbstractAccess<>(Client.class).getColumnByID(id);
                    new ClientUpdateUI(c);
                    frame.dispose();

                }
            }
        });
    }

    protected class Task extends SwingWorker<String, String> {
        protected int refresh = 0;
        @Override
        protected String doInBackground() throws Exception {

            String[] arg = new String[Client.class.getDeclaredFields().length];
            int i = 0;
            for (Field field : Client.class.getDeclaredFields()) {
                arg[i++] = field.getName();
            }

            clients = new JTable();
            int rows = new ClientBusiness().getNumberOfClients();
            DefaultTableModel model = new DefaultTableModel(arg, rows) {

                @Override
                public boolean isCellEditable(int row, int col) {
                    return false;
                }

                @Override
                public Class<?> getColumnClass(int col) {
                    return String.class;
                }
            };

            int index = 0;
            for (int j = 1; j <= rows; j++) {
                Client client = new AbstractAccess<>(Client.class).getColumnByID(j);
                if (!client.getClientName().equals("null")) {
                    model.setValueAt(client.getID(), index, 0);
                    model.setValueAt(client.getClientName(), index, 1);
                    model.setValueAt(client.getClientEmail(), index, 2);
                    model.setValueAt(client.getClientPhoneNumber(), index, 3);
                    model.setValueAt(client.getClientRegistrationDate(), index, 4);
                    index++;
                }
            }

            clients.setModel(model);
            clients.setBounds(30, 50, 600, 200);
            clients.setPreferredScrollableViewportSize(new Dimension(600, 200));

            DefaultTableCellRenderer cellRenderer = new DefaultTableCellRenderer();
            cellRenderer.setHorizontalAlignment(JLabel.CENTER);
            clients.setCellSelectionEnabled(false);
            clients.setRowSelectionAllowed(true);
            clients.setDefaultRenderer(String.class, cellRenderer);
            clients.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
            clients.getTableHeader().setReorderingAllowed(false);
            frame.add(clients);

            JScrollPane pane = new JScrollPane(clients);
            pane.setBounds(30, 50, 610, 200);
            pane.setVisible(true);
            frame.add(pane);
            if (refresh == 1)
                model.setRowCount(0);

            return null;
        }
    }
}
