package presentation;

import businessLogicLayer.ProductBusiness;
import dataAccesLayer.AbstractAccess;
import dataAccesLayer.ProductData;
import model.Client;
import model.Product;

import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.lang.reflect.Field;
import java.util.*;
import java.util.List;

class ProductsUI {
    private int clientID;
    private JFrame frame;
    private JTable products;
    private int nextFrame;

    ProductsUI(int clientID, int nextFrame) {
        this.nextFrame = nextFrame;
        this.clientID = clientID;
        EventQueue.invokeLater(ProductsUI.this::initialize);
    }

    private void initialize() {
        frame = new JFrame();
        frame.setLayout(null);
        frame.setBounds(100, 100, 700, 400);
        frame.setTitle("Product List");
        frame.setVisible(true);
        editProductsAdmin();

        JTextField clientInfo = new JTextField();
        clientInfo.setBounds(0, 0, 700, 25);
        Client client = new AbstractAccess<>(Client.class).getColumnByID(clientID);
        clientInfo.setText(client.toString());
        clientInfo.setHorizontalAlignment(JTextField.CENTER);
        clientInfo.setEditable(false);
        frame.add(clientInfo);

        JButton order = new JButton("Order");
        order.setBounds(30, 310, 150, 30);
        frame.add(order);

        order.addActionListener(e -> {
            List<Product> productList = new ArrayList<>();
            int length = products.getRowCount();
            for (int i = 1; i <= length; i++) {
                if (products.isRowSelected(i - 1)) {
                    Product product = new Product();
                    product.setId(Integer.parseInt(products.getModel().getValueAt(i - 1, 0).toString()));
                    productList.add(new ProductBusiness().getProduct(product));
                }
            }
            if (!productList.isEmpty()) {
                new OrderUI(productList, clientID);
                frame.dispose();
            }
        });

        JButton back = new JButton("Back");
        back.setBounds(550, 310, 120, 30);
        frame.add(back);
        back.addActionListener(e -> {
            frame.dispose();
            if (nextFrame == 0)
                new LoginUI().initialize();
        });

        new Task().execute();
    }

    private void editProductsAdmin() {
        if (clientID == 1) {
            JButton addProduct = new JButton("Add Product");
            addProduct.setBounds(30, 260, 150, 30);
            frame.add(addProduct);

            addProduct.addActionListener(e -> {
                frame.dispose();
                new AddProductUI(clientID);
            });

            JButton deleteProduct = new JButton("Delete Selected Product");
            deleteProduct.setBounds(200, 260, 200, 30);
            frame.add(deleteProduct);
            deleteProduct.addActionListener(e -> {
                Product product = new Product();
                if (products.getSelectedRow() > 0) {
                    try {
                        product.setId(Integer.parseInt(products.getModel().getValueAt(products.getSelectedRow(), 0).toString()));
                    } catch (NullPointerException ex) {
                        System.out.println("Select a valid product to delete.");
                    }
                    if (new ProductBusiness().deleteProduct(product)) {
                        frame.dispose();
                        new ProductsUI(clientID, nextFrame);
                    }
                } else {
                    System.out.println("Select a product to delete");
                }
            });

            JButton updateProduct = new JButton("Update Product");
            updateProduct.setBounds(420, 260, 150, 30);
            frame.add(updateProduct);
            updateProduct.addActionListener(e -> {
                Thread th = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        UpdateProductUI upui = new UpdateProductUI();
                        while (true) {
                            if (upui.closeFrame == 1) {
                                frame.dispose();
                                break;
                            }
                        }
                    }
                });
                th.start();

            });
        }
    }

    protected class Task extends SwingWorker<String, String> {
        int refresh = 0;

        @Override
        protected String doInBackground() {

            String[] arg = new String[Product.class.getDeclaredFields().length];
            int i = 0;
            for (Field field : Product.class.getDeclaredFields()) {
                arg[i++] = field.getName();
            }

            products = new JTable();
            int rows = new ProductBusiness().getNumber();
            DefaultTableModel model = new DefaultTableModel(arg, rows);

            int index = 0;
            for (int j = 1; j <= rows; j++) {
                Product product = new AbstractAccess<>(Product.class).getColumnByID(j);
                if (product.getProductQuantity() > 0) {
                    model.setValueAt(product.getId(), index, 0);
                    model.setValueAt(product.getProductName(), index, 1);
                    model.setValueAt(product.getProductQuantity(), index, 2);
                    model.setValueAt(product.getProductPrice(), index, 3);
                    index++;
                }
            }

            products.setModel(model);
            products.setBounds(30, 50, 600, 200);
            products.setPreferredScrollableViewportSize(new Dimension(600, 200));

            DefaultTableCellRenderer cellRenderer = new DefaultTableCellRenderer();
            cellRenderer.setHorizontalAlignment(JLabel.CENTER);
            products.setCellSelectionEnabled(false);
            products.setRowSelectionAllowed(true);
            products.setDefaultRenderer(String.class, cellRenderer);
            products.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
            products.getTableHeader().setReorderingAllowed(false);
            frame.add(products);

            JScrollPane pane = new JScrollPane(products);
            pane.setBounds(30, 50, 610, 200);
            pane.setVisible(true);
            frame.add(pane);

            if (refresh == 1) {
                model.setRowCount(0);
            }

            return null;
        }
    }
}
