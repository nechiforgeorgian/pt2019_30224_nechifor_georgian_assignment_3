package presentation;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class AdminUI {

    AdminUI() {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                initialize();
            }
        });
    }

    private void initialize() {
        final JFrame frame = new JFrame();
        frame.setTitle("Admin");
        frame.setBounds(100, 100, 400, 200);
        frame.setLayout(null);
        frame.setVisible(true);

        JTextField user = new JTextField("You are logged in as Admin");
        user.setHorizontalAlignment(JLabel.CENTER);
        user.setEditable(false);
        user.setBounds(0, 0, 400, 20);
        frame.add(user);

        JButton clients = new JButton("Clients");
        clients.setBounds(30, 30, 150, 40);
        frame.add(clients);

        JButton products = new JButton("Products");
        products.setBounds(200, 30, 150, 40);
        frame.add(products);

        JButton back = new JButton("Back");
        back.setBounds(140, 100, 100, 30);
        frame.add(back);

        back.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                frame.dispose();
                new LoginUI().initialize();
            }
        });

        products.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new ProductsUI(1, 10);
            }
        });

        clients.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new ClientsUI();
            }
        });
    }
}
