package presentation;

import businessLogicLayer.OrderBusiness;
import model.Client;
import model.OrderDetails;
import model.Product;

import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

public class OrderUI {
    private List<Product> productList;
    private int clientID;
    private List<OrderDetails> orderDetails = new ArrayList<>();
    private JFrame frame;
    private JTable products;

    OrderUI(List<Product> productList, int clientID) {
        this.productList = productList;
        this.clientID = clientID;
        EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                initialize();
            }
        });
    }

    private void initialize() {
        frame = new JFrame();
        frame.setTitle("Order Details");
        frame.setLayout(null);
        frame.setVisible(true);
        frame.setBounds(100, 100, 700, 400);

        Task task = new Task();
        task.execute();

        JButton order = new JButton();
        order.setBounds(30, 260, 150, 30);
        order.setText("Finish order");
        frame.add(order);
        order.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int len = productList.size();
                for (int i = 0; i < len; i++) {
                    if (Integer.parseInt(products.getModel().getValueAt(i, 2).toString()) > 0) {
                        int id = Integer.parseInt(products.getModel().getValueAt(i, 0).toString());
                        int amount = Integer.parseInt(products.getModel().getValueAt(i, 4).toString());
                        orderDetails.add(new OrderDetails(0, 0, id, amount));
                    } else {
                        JOptionPane.showMessageDialog(frame, "Invalid Order", "Error", JOptionPane.ERROR_MESSAGE);
                        orderDetails.clear();
                        break;
                    }
                }
                if (!orderDetails.isEmpty()) {
                    Client client = new Client();
                    client.setID(clientID);
                    new OrderBusiness().addOrder(client, orderDetails);
                    JOptionPane.showMessageDialog(frame, "Order completed!", "Success", JOptionPane.INFORMATION_MESSAGE);
                    frame.dispose();
                    new ProductsUI(clientID, 1);
                }
            }
        });

        JButton back = new JButton("Back");
        back.setBounds(550, 260, 120, 30);
        frame.add(back);
        back.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                frame.dispose();
                new ProductsUI(clientID, 1);
            }
        });
    }

    private class Task extends SwingWorker<String, String> {
        @Override
        protected String doInBackground() {

            String[] arg = new String[Product.class.getDeclaredFields().length + 1];
            int i = 0;
            for (Field field : Product.class.getDeclaredFields()) {
                arg[i++] = field.getName();
            }
            arg[i] = "Amount";

            int rows = productList.size();
            DefaultTableModel model = new DefaultTableModel(arg, rows) {

                @Override
                public boolean isCellEditable(int row, int col) {
                    if (col != 4)
                        return false;
                    return true;
                }

                @Override
                public Class<?> getColumnClass(int col) {
                    return String.class;
                }
            };
            int index = 0;
            for (int j = 1; j <= rows; j++) {
                Product product = productList.get(j - 1);
                if (product.getProductQuantity() > 0) {
                    model.setValueAt(product.getId(), index, 0);
                    model.setValueAt(product.getProductName(), index, 1);
                    model.setValueAt(product.getProductQuantity(), index, 2);
                    model.setValueAt(product.getProductPrice(), index, 3);
                    model.setValueAt(1, index, 4);
                    index++;
                }
            }
            products = new JTable();
            products.setModel(model);
            products.setBounds(30, 20, 630, 200);
            products.setPreferredScrollableViewportSize(new Dimension(630, 200));

            DefaultTableCellRenderer cellRenderer = new DefaultTableCellRenderer();
            cellRenderer.setHorizontalAlignment(JLabel.CENTER);
            products.setDefaultRenderer(String.class, cellRenderer);
            products.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
            products.getTableHeader().setReorderingAllowed(false);
            frame.add(products);

            JScrollPane pane = new JScrollPane(products);
            pane.setBounds(30, 50, 630, 200);
            pane.setVisible(true);
            frame.add(pane);

            return null;
        }
    }
}
