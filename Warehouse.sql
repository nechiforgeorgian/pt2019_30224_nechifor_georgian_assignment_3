drop database warehouse;
create database if not exists warehouse;
use warehouse;

create table Product (
	ID INT auto_increment primary key,
    productName VARCHAR(30),
    productQuantity INT,
    productPrice FLOAT);
    
create table orderDetails (
	ID INT auto_increment primary key,
    orderID INT,
    productID INT,
    amount INT);
    
create table _order (
	ID INT auto_increment primary key,
    clientID INT, 
    orderCost FLOAT,
    orderDate Date);
    
create table _user (
	ID INT auto_increment primary key,
    clientID INT,
    username VARCHAR(20) unique,
    passwd varchar(20));
    
create table client (
	ID INT auto_increment primary key,
    clientName VARCHAR(40),
    clientEmail VARCHAR(40),
    clientPhoneNumber VARCHAR(10),
    clientRegistrationDate Date);
    
alter table orderDetails
add foreign key (productID) references Product(ID) on delete cascade,
add foreign key (orderID) references _order(ID) on delete cascade;

alter table _order
add foreign key (clientID) references client(ID) on delete cascade;

alter table _user
add foreign key (clientID) references client(ID) on delete cascade;

insert into Product (productName, productQuantity, productPrice)
values ("Calculatoare", 50, 1250.50),
	   ("Laptopuri", 14, 2339.99),
	   ("Ciment", 80, 120.00),
	   ("Cutzite", 54, 331.99),
	   ("Prize de 12v", 140, 32.99),
	   ("Corn Ciocolata MAX", 22, 6.99),
	   ("V la punga", 22, 10.99),
	   ("Franzela", 220, 0.99),
	   ("Audi RS7", 1, 6601603.99),
	   ("IPhone XS Max", 22, 5693.99),
	   ("Bomboane cu aur", 19, 1233.99),
	   ("Clienti de magazin", 12, 613.99),
	   ("Cadou frumos", 92, 43.99),
	   ("Apa cuantica", 11, 33.99),
	   ("Reteta de prajituri", 52, 23.99),
	   ("Lapte gros", 22, 13.99);

insert into client (clientName, clientEmail, clientPhoneNumber, clientRegistrationDate)
values
	("Georgian Nechifor", 'nechiforgeorgian@gmail.com', '0756968861', "2017-03-17"),
	("Vasile Traian", 'organeledeinterne@gmail.com', '0731492100', "2018-03-21"),
	("Hakuna Matata", 'hakunamatata@gmail.com', '0800800790', "2019-04-11");


insert into _user(clientID, username, passwd)
values (1, "admin", "admin"),
	   (2, "vasile", "traian"),
	   (3, "hakuna", "matata");

delimiter // 
create procedure login(IN userName varchar(30), IN passwd varchar(30), OUT valid INT)
begin
	SET @UN = NULL, @PSW = NULL, @ID = NULL, @AUX = NULL;
    SELECT @UN := _user.username, @PSW := _user.passwd, @ID := _user.clientID from _user where _user.username = userName;
    if(@UN IS NOT NULL) then
		if(@PSW = passwd) then
			set @AUX = 1;
		else
			set @AUX = 0;
		end if;
	else
		set @AUX = 0;
	end if;
    
    if @AUX = 1 then
		set valid = @ID;
	else
		set valid = -1;
	end if;
END //
delimiter ;


select * from _order;
select * from product where product.ID = 1;
select * from product;
select * from client;


    
    
    
    